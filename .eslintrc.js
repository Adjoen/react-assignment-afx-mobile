module.exports = {
    root: true,
    extends: '@react-native-community',
    rules: {
        eqeqeq: 0,
        'no-return-assign': 0,
        'react-native/no-inline-styles': 0,
        'prettier/prettier': [
            'error',
            {
                endOfLine: 'auto',
            },
        ],
    },
};
