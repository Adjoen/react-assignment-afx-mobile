import {useTheme} from '@react-navigation/native';
import React, {useEffect} from 'react';
import {Text} from 'react-native';
import {FONT, FONT_SIZE} from '../styles';

/**
 *
 * @param {Object} props
 * @param {String} [props.color = "black"]
 * @param {("bold" | "boldItalic" | "extraBold" | "extraBoldItalic" | "italic" | "light" | "lightItalic" | "medium" | "mediumItalic" | "regular" | "semiBold" | "semiBoldItalic" )} [props.weight = "medium"]
 * @param {("xs" | "sm" | "md" | "lg" | "xl" | "2xl" | "3xl" | "4xl" | "5xl")} [props.size = 'md']
 * @param {("left" | "center" | "right")} [props.align = 'left']
 * @param {Number} [props.numberOfLines]
 * @param {("head" | "middle" | "tail" | "clip")} [props.ellipsizeMode = 'tail']
 * @param {import('react-native').TextStyle} [props.style]
 */
const CustomText = ({
  color = 'black',
  weight = 'medium',
  size = 'md',
  align = 'left',
  style,
  onPress,
  children,
  numberOfLines,
  ellipsizeMode,
}) => {
  const [isMounted, setIsMounted] = React.useState(false);
  const {colors} = useTheme();
  useEffect(() => {
    setIsMounted(true);

    return () => {
      setIsMounted(false);
    };
  }, []);

  const styles = {
    color: colors[color]
      ? colors[color]
      : typeof color === 'string'
      ? color
      : colors.black,
    fontFamily: FONT[weight],
    fontSize: FONT_SIZE?.[size]
      ? FONT_SIZE[size]
      : typeof size === 'string'
      ? parseInt(size, 10)
      : size,
    textAlign: align,
  };

  return !isMounted ? null : (
    <Text
      onPress={onPress}
      style={[styles, style]}
      numberOfLines={numberOfLines}
      ellipsizeMode={ellipsizeMode}
    >
      {children}
    </Text>
  );
};
export default CustomText;
