import Geolocation from '@react-native-community/geolocation';
import {useTheme} from '@react-navigation/native';
import React, {useCallback, useEffect, useState} from 'react';
import {PermissionsAndroid, View} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import {FokusAwareStatusBar} from '../components';
import _ from '../styles';
import {SearchBox} from './_components';
import {isEmpty} from '../utils';

const Index = () => {
    const {colors} = useTheme();

    const defaultCamera = {
        center: {
            latitude: -6.2295712,
            longitude: 106.7594781,
        },
        zoom: 11,
        altitude: 1,
        pitch: 1,
        heading: 1,
    };

    const defaultZoom = 11;

    const [camera, setCamera] = useState(defaultCamera);
    const [currentRegion, setCurrentRegion] = useState({});
    const [selectedLocation, setSelectedLocation] = useState({});

    const showCurrentRegion = currentRegion.latitude > 0 && currentRegion.longitude > 0;

    const updateCamera = v => setCamera(s => ({...s, ...v}));

    const changeLocation = v => {
        const newLocation = {
            center: {
                latitude: v.geometry.location.lat,
                longitude: v.geometry.location.lng,
            },
        };

        updateCamera(newLocation);
        setSelectedLocation(newLocation.center);
    };

    useEffect(() => {
        requestPermission();
    }, [requestPermission]);

    const getCurrentLocation = async () => {
        const onSuccess = ({coords = {}}) => {
            if (coords.latitude && coords.longitude) {
                const newRegion = {
                    latitude: coords.latitude,
                    longitude: coords.longitude,
                };

                updateCamera({center: newRegion, zoom: defaultZoom});
                setCurrentRegion(newRegion);
            }
        };

        const onError = e => {
            console.log(e);
        };

        const options = {
            timeout: 5000,
            maximumAge: 100,
            enableHighAccuracy: true,
        };

        Geolocation.getCurrentPosition(onSuccess, onError, options);
    };
    const getCurrentLocationCallback = useCallback(getCurrentLocation, []);

    const requestPermission = useCallback(() => {
        const initialize = async () => {
            try {
                const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    getCurrentLocationCallback();
                } else {
                    console.log('Camera permission denied');
                }
            } catch (err) {
                console.warn(err);
            }
        };
        initialize();
    }, [getCurrentLocationCallback]);

    async () => {};

    return (
        <>
            <FokusAwareStatusBar translucent={false} backgroundColor={colors.background} />
            <View style={_.flex}>
                <MapView style={_.flex} initialCamera={defaultCamera} camera={camera}>
                    {showCurrentRegion && (
                        <Marker
                            coordinate={currentRegion}
                            style={{
                                height: 24,
                                width: 24,
                            }}>
                            <View style={[_['border-white'], _.overflowHidden, _['rounded-full'], _['border-4'], _.boxShadow]}>
                                <View style={[_.bgPrimaryLight, _['h-full'], _['w-full']]} />
                            </View>
                        </Marker>
                    )}
                    {!isEmpty(selectedLocation) && <Marker coordinate={selectedLocation} />}
                </MapView>
                <SearchBox changeLocation={changeLocation} clearLocation={() => setSelectedLocation({})} />
            </View>
        </>
    );
};

export default Index;
