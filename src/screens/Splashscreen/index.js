import {View, ActivityIndicator} from 'react-native';
import React, {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import {passLoading} from '../../redux/actions/auth';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {Card, Text, FokusAwareStatusBar} from '../../components';
import _ from '../../styles';
import {useTheme} from '@react-navigation/native';
const Index = () => {
    const {styles, colors} = useTheme();
    const dispatch = useDispatch();

    useEffect(() => {
        setTimeout(() => {
            dispatch(passLoading());
        }, 3000);
    }, [dispatch]);

    return (
        <>
            <FokusAwareStatusBar translucent={false} backgroundColor={colors.background} />
            <View style={[_.flex, _.p, styles.background]}>
                <Card style={[_.flex]}>
                    <View style={_.mvAuto}>
                        <FontAwesomeIcon icon="map-location-dot" style={[_.mhAuto, _.mb]} size={200} color="#006E7F" />

                        <Text color="text" align="center" size="lg" weight="bold" style={_.mb}>
                            React Assignment AFX
                        </Text>
                        <View style={[_.row, _.itemsCenter, _.contentCenter]}>
                            <ActivityIndicator color={colors.primary} />
                            <Text color="text" style={_.ml_1}>
                                Loading...
                            </Text>
                        </View>
                    </View>
                </Card>
            </View>
        </>
    );
};

export default Index;
