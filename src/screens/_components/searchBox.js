import {View, TouchableOpacity, ScrollView} from 'react-native';
import React, {forwardRef, useImperativeHandle, useRef, useState} from 'react';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import _, {FONT} from '../../styles';
import {useTheme} from '@react-navigation/native';
import {Card, Text} from '../../components';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faTimes} from '@fortawesome/free-solid-svg-icons/faTimes';
import {faHistory} from '@fortawesome/free-solid-svg-icons/faHistory';
import {useDispatch, useSelector} from 'react-redux';
import {addPlace, removePlace} from '../../redux/actions/places';

const Index = ({changeLocation, clearLocation}) => {
    const {colors, styles} = useTheme();
    const [value, setValue] = useState('');
    const dispatch = useDispatch();

    const boxRef = useRef();
    const historyRef = useRef();

    const clearSearch = () => {
        setValue('');
        boxRef.current.blur();
        boxRef.current.setAddressText('');
        clearLocation();
        historyRef.current.hide();
    };

    return (
        <View style={[_.absolute, _['top-0'], _['left-0'], _['right-0'], _.p]}>
            <GooglePlacesAutocomplete
                ref={boxRef}
                onFail={console.log}
                placeholder="Telusuri Peta"
                minLength={2}
                autoFocus={false}
                returnKeyType={'default'}
                query={{
                    key: 'AIzaSyDTfcknlZY-Rdq4S28Mk5rAcyO79ds5Q9E',
                    language: 'id',
                }}
                styles={{
                    listView: {
                        backgroundColor: '#000',
                    },
                    textInputContainer: [
                        {
                            backgroundColor: colors.container,
                        },
                        _['rounded-xs'],
                        _.overflowHidden,
                        _.row,
                        _.itemsCenter,
                    ],
                    row: {
                        backgroundColor: colors.container,
                    },
                    description: {
                        color: colors.text,
                        fontFamily: FONT.regular,
                        fontSize: 14,
                    },
                    poweredContainer: [styles.background],
                    separator: {
                        backgroundColor: colors.grey,
                    },
                    textInput: [styles.container, {color: colors.text, fontFamily: FONT.regular, fontSize: 18}],
                }}
                fetchDetails
                listViewDisplayed="auto"
                currentLocation={false}
                textInputProps={{value, onChangeText: setValue}}
                listEmptyComponent={() => (
                    <Card>
                        <Text color="text" align="center">
                            Lokasi tidak ditemukan
                        </Text>
                    </Card>
                )}
                renderRightButton={() => (
                    <>
                        {value !== '' && (
                            <TouchableOpacity style={[_.p, _.pl_0]} onPress={clearSearch}>
                                <FontAwesomeIcon icon={faTimes} color={colors.text} />
                            </TouchableOpacity>
                        )}
                        <TouchableOpacity style={[_.p, _.pl_0]} onPress={() => historyRef.current.show()}>
                            <FontAwesomeIcon icon={faHistory} color={colors.text} />
                        </TouchableOpacity>
                    </>
                )}
                onPress={(data, details = null) => {
                    setValue(details.formatted_address);
                    boxRef.current.setAddressText(details.formatted_address);
                    dispatch(addPlace(details));
                    changeLocation(details);
                    historyRef.current.hide();
                }}
            />
            <History onChange={changeLocation} ref={historyRef} />
        </View>
    );
};

export default Index;

const History = forwardRef(({onChange}, ref) => {
    const [show, setShow] = useState(false);
    const places = useSelector(s => s.places);
    const dispatch = useDispatch();

    const {colors, styles} = useTheme();
    useImperativeHandle(
        ref,
        () => ({
            show() {
                setShow(s => !s);
            },
            hide() {
                setShow(false);
            },
        }),
        []
    );

    const deletePlace = place_id => {
        dispatch(removePlace({place_id}));
        setShow(false);
    };

    const length = Object.keys(places).length;

    return (
        show && (
            <ScrollView style={[{maxHeight: 300}, styles.card, _['rounded-t-none'], _.overflowHidden]}>
                {length > 0 ? (
                    Object.keys(places).map((x, i) => (
                        <View style={[_.row, _.itemsCenter, _.mb]}>
                            <TouchableOpacity style={[_.row, _.itemsCenter, _.flex]} onPress={() => onChange(places[x])}>
                                <FontAwesomeIcon style={{marginRight: 20}} icon={faHistory} color={colors.text} />
                                <Text color="text" style={_.flex}>
                                    {places[x].formatted_address}
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => deletePlace(x)}>
                                <FontAwesomeIcon style={{marginLeft: 20}} icon={faTimes} color={colors.text} />
                            </TouchableOpacity>
                        </View>
                    ))
                ) : (
                    <Text align="center" color="text">
                        Belum ada tempat yang sudah pernah dicari
                    </Text>
                )}
            </ScrollView>
        )
    );
});
