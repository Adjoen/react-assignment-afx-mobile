import {View} from 'react-native';
import React, {forwardRef, useImperativeHandle, useState} from 'react';
import {Marker} from 'react-native-maps';
import _ from '../../styles';

const Index = forwardRef(({}, ref) => {
    const [show, setShow] = useState(false);
    const [coordinate, setcoordinate] = useState({
        latitude: 0,
        longitude: 0,
    });

    useImperativeHandle(
        ref,
        () => ({
            show(e) {
                setcoordinate(e);
                setShow(true);
            },
        }),
        []
    );
    return (
        <>
            {show && (
                <Marker
                    coordinate={coordinate}
                    style={{
                        height: 24,
                        width: 24,
                    }}>
                    <View style={[_['border-white'], _.overflowHidden, _['rounded-full'], _['border-4'], _.boxShadow]}>
                        <View style={[_.bgPrimaryLight, _['h-full'], _['w-full']]} />
                    </View>
                </Marker>
            )}
        </>
    );
});

export default Index;
