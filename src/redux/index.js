import {applyMiddleware, combineReducers, createStore} from '@reduxjs/toolkit';
import thunk from 'redux-thunk';
import authReducers from './reducers/auth';
import placesReducers from './reducers/places';
import themeReducers from './reducers/theme';

const rootReducer = combineReducers({
    auth: authReducers,
    theme: themeReducers,
    places: placesReducers,
});
export const store = createStore(rootReducer, applyMiddleware(thunk));
