export const AUTH_LOADING_PASS = 'AUTH_LOADING_PASS';

export const THEME_PATCH = 'THEME_PATCH';

export const PLACE_ADD = 'PLACE_ADD';
export const PLACE_REMOVE = 'PLACE_REMOVE';
