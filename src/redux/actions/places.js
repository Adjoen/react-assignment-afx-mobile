import {PLACE_ADD, PLACE_REMOVE} from '../type';

export function addPlace(value = {}) {
    return {
        type: PLACE_ADD,
        value,
    };
}
export function removePlace(value) {
    return {
        type: PLACE_REMOVE,
        value,
    };
}
