import {THEME_PATCH} from '../type';

export const patchTheme = (value = {}) => {
  return {
    type: THEME_PATCH,
    value,
  };
};
