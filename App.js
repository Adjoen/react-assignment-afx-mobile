import {library} from '@fortawesome/fontawesome-svg-core';
import {faMapLocationDot} from '@fortawesome/free-solid-svg-icons/faMapLocationDot';

import React from 'react';
import {Provider} from 'react-redux';
import Navigator from './navigator';
import {store} from './src/redux';

library.add(faMapLocationDot);
const Index = () => {
    return (
        <Provider store={store}>
            <Navigator />
        </Provider>
    );
};

export default Index;
